<?php

/**
 * @file
 * This file contains API functions for the reviews system
 */

/**
 * Helper function returns TRUE or FALSE based on whether the current user
 * has reviewed the node being displayed. If admin setting is set to allow
 * multiple reviews from a single user then we return FALSE immediately.
 */
function _check_user_review($nid, $uid) {
  if (variable_get('reviews_allow_multiple', 0)) {
    return FALSE;
  }

  $results = db_select('reviews', 'r')
    ->fields('r')
    ->condition('uid', $uid, '=')
    ->condition('nid', $nid, '=')
    ->execute()
    ->fetchAll();
  if (count($results) != 0) {
    return TRUE;
  } else {
    return FALSE;
  }
}

/**
 * Helper function return TRUE or FALSE based on whether reviews are enabled.
 */
function _reviews_enabled() {
  if (variable_get('reviews_enabled', 0) == 0) {
    return FALSE;
  } else {
    return TRUE;
  }
}

/**
 * Helper function to return the number of approved reviews for the
 * current node.
 */
function _reviews_count($nid) {
  $results = db_select('reviews', 'r')
    ->fields('r')
    ->condition('nid', $nid, '=')
    ->condition('status', 1, '=')
    ->execute()
    ->fetchAll();
  return count($results);
}

/**
 * Helper function to return the number of pending reviews for the
 * current node.
 */
function _reviews_pending_count($nid) {
  $results = db_select('reviews', 'r')
    ->fields('r')
    ->condition('nid', $nid, '=')
    ->condition('status', 0, '=')
    ->execute()
    ->fetchAll();
  return count($results);
}

/**
 * Helper function to return a username from a user ID.
 */
function _get_username($uid) {
  $user = user_load($uid);
  if ($user) {
    return $user->name;
  } else {
    return 'Anonymous';
  }
}

/**
 * Helper function to return a node title from a node ID.
 */
function _get_node_title($nid) {
  $node = node_load($nid);
  if ($node) {
    return $node->title;
  } else {
    return 'Unknown';
  }
}

/**
 * Helper function to return the review status name form the ID.
 */
function _get_review_status($sid) {
  $statuses = array(
    0 => '<span class="marker">' . t('Awaiting moderation') . '</span>',
    1 => t('Published'),
    2 => t('Rejected'),
  );
  return $statuses[$sid];
}
