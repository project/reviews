<?php

/**
 * @file
 * This file contain all function necessary for displaying and
 * leaving reviews.
 */

/**
 * Gather reviews for current displayed node and pass to theming function.
 */
function reviews_get_reviews($nid) {
  $reviews_count = _reviews_count($nid);
  $pending_count = _reviews_pending_count($nid);

  if ($reviews_count == 0 && !user_access('administer reviews')) {
    return theme('reviews-no-reviews', array('nid' => $nid));
  }

  // Get the sort order for reviews.
  if (variable_get('reviews_sort_order', 0) == 0) {
    $sort = 'ASC';
  } else {
    $sort = 'DESC';
  }

  if (user_access('administer reviews')) {
    $operand = '<=';
  } else {
    $operand = '=';
  }

  // Get the reviews from the database.
  $reviews = db_select('reviews', 'r')
    ->fields('r')
    ->condition('nid', $nid, '=')
    ->condition('status', 1, $operand)
    ->orderBy('created', $sort)
    ->execute()
    ->fetchAll();

  return theme('reviews', array('nid' => $nid, 'review_count' => $reviews_count, 'pending_count' => $pending_count, 'reviews' => $reviews));
}

/**
 * Form builder for add review page.
 */
function reviews_add_review($form, &$form_state, $nid) {
  global $user;

  $form = array();

  $node = node_load($nid);
  $title = $node->title;

  $form['intro'] = array(
    '#type' => 'item',
    '#markup' => t('Complete the form below to leave a review of <em>!title</em>', array('!title' => check_plain($title))),
  );

  $form['nid'] = array(
    '#type' => 'hidden',
    '#value' => $nid,
  );

  $form['uid'] = array(
    '#type' => 'hidden',
    '#value' => $user->uid,
  );

  $form['review'] = array(
    '#type' => 'text_format',
    '#title' => t('Your review'),
    '#rows' => 10,
    '#required' => TRUE,
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Submit'));

  return $form;
}

/**
 * Submit handler for reviews_add_review form.
 */
function reviews_add_review_submit($form, &$form_state) {
  $nid      = $form_state['values']['nid'];
  $uid      = $form_state['values']['uid'];
  $review   = $form_state['values']['review'];
  $created  = REQUEST_TIME;
  $record   = array(
    'nid' => $nid,
    'uid' => $uid,
    'review' => serialize($review),
    'status' => 0,
    'created' => $created,
  );

  $result = drupal_write_record('reviews', $record);

  if ($result == SAVED_NEW) {
    drupal_set_message(t('Your review has been submitted. It will be moderated before it is published.'), 'status');
    drupal_goto('node/' . $nid);
  } else {
    drupal_set_message(t('There seems to have been a problem submitting your review. Please try again later'), 'error');
    drupal_goto('node/' . $nid . '/add-review');
  }
}
