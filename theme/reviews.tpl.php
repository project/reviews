<?php

/**
 * @file
 * Theme template file used to format the reviews page of enabled content
 * types when there are reviews.
 * $variables:
 *  $nid: the node ID of the main node being viewed.
 *  $review_count: the number of published reviews.
 *  $pending_count: the number of reviews awaiting moderation.
 *  $reviews: array of reviews.
 */

?>
<div class="reviews">

  <p>
    The following reviews have been left for this content:
  </p>

  <?php

    foreach ($variables['reviews'] as $index => $review) {
      print theme('review', array('nid' => $variables['nid'], 'index' => $index, 'total_reviews' => $variables['review_count'], 'review' => $review));
    }

  ?>

  <?php if (count($reviews) != 0) { ?>
    <div class="reviews-published-count">
      <?php if ($variables['review_count'] == 1) {print t('There is 1 published review for this content.');} ?>
    </div>
    <div class="reviews-published-count">
      <?php if ($variables['review_count'] != 1) {print t('There are !num published reviews for this content.', array('!num' => $variables['review_count']));} ?>
    </div>
    <div class="reviews-pending-count">
      <?php if ($variables['pending_count'] == 1) {print t('There is 1 pending review for this content.');} ?>
    </div>
    <div class="reviews-pending-count">
      <?php if ($variables['pending_count'] != 1) {print t('There are !num pending reviews for this content.', array('!num' => $variables['pending_count']));} ?>
    </div>
  <?php } ?>

  <div class="reviews-back-to-page">
    <?php print l('Back to page', 'node/' . $variables['reviews'][0]->nid); ?>
  </div>

</div>
