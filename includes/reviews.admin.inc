<?php

/**
 * @file
 * This file contains all the functions necessary for the admin
 * pages of the reviews system.
 */

/**
 * Form builder for the main settings page.
 */
function reviews_settings($form, &$form_state) {
  $form = array();

  $form['main'] = array(
    '#type' => 'fieldset',
    '#title' => 'Enable/Disable',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['main']['reviews_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enabled'),
    '#default_value' => variable_get('reviews_enabled', ''),
    '#description' => t('Turn the review system on or off. When the reviews system is turned off all existing reviews data is retained but the ability to leave or view reviews is disabled.'),
  );

  $form['main']['reviews_sort_order'] = array(
    '#type' => 'select',
    '#title' => t('Sort order for displaying reviews'),
    '#options' => array(0 => 'Oldest first.', 1 => 'Newest first.'),
    '#default_value' => variable_get('reviews_sort_order', 0),
  );

  $form['main']['reviews_allow_multiple'] = array(
    '#type' => 'select',
    '#title' => t('Allow multiple reviews from a single user'),
    '#options' => array(0 => 'No', 1 => 'Yes'),
    '#default_value' => variable_get('reviews_allow_multiple', 0),
  );

  $form['ctypes'] = array(
    '#type' => 'fieldset',
    '#title' => 'Reviewable Content Types',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['ctypes']['reviews_enabled_content_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Available Content Types'),
    '#options' => _get_node_types(),
    '#default_value' => variable_get('reviews_enabled_content_types', array()),
  );

  return system_settings_form($form);
}

/**
 * Helper function to return an array of available content types.
 */
function _get_node_types() {
  $types = node_type_get_types();
  $tmp = array();
  foreach ($types as $key => $type) {
    $tmp[$key] = $type->name;
  }
  return $tmp;
}

/**
 * Form builder for reviews moderation.
 */
function reviews_list($form, &$form_state) {
  $form = array();

  // Check for confirmation forms.
  if (isset($form_state['confirm_delete'])) {
    return array_merge($form, reviews_list_confirm_delete($form, $form_state));
  }

  $result = db_select('reviews', 'r')->extend('PagerDefault')->limit(15)
    ->fields('r')
    ->orderBy('created', 'ASC')
    ->execute();

  $header = array(
    'rid' => array('data' => t('Review ID')),
    'node_title' => array('data' => t('Reviewed Content Title')),
    'username' => array('data' => t('Reviewer')),
    'review' => array('data' => t('Review')),
    'status' => array('data' => t('Status')),
    'actions' => array('data' => t('Operations')),
  );

  $data = array();
  while ($review = $result->fetchAssoc()) {
    $review_content = unserialize($review['review']);

    $links['items'] = array(
      l('view', 'node/'. $review['nid'] .'/reviews', array('fragment' => 'review_' . $review['rid'])),
    );

    $data[$review['rid']] = array(
      'rid' => $review['rid'],
      'node_title' => check_plain(_get_node_title($review['nid'])),
      'username' => check_plain(_get_username($review['uid'])),
      'review' => check_markup($review_content['value'], $review_content['format']),
      'status' => _get_review_status($review['status']),
      'actions' => theme('item_list', $links),
    );
  }

  $form['reviews'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $data,
    '#empty' => t('There are currently no reviews.'),
  );

  $form['pager'] = array(
    '#markup' => theme('pager', array('tags' => array())),
  );

  $form['actions']['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
    '#access' => user_access('administrator reviews'),
    '#weight' => 10,
  );

  return $form;
}

/**
 * Confirm handler for review_list form.
 */
function reviews_list_submit($form, &$form_state) {
  if ($form_state['triggering_element']['#value'] == t('Delete')) {
    // Delete the selected reviews.
    if ($form_state['values']['delete'] === TRUE) {
      return reviews_list_confirm_delete_submit($form, $form_state);
    }
    // Rebuild the form to confirm review deletion.
    $form_state['rebuild'] = TRUE;
    $form_state['confirm_delete'] = TRUE;
    return;
  }
}

/**
 * Confirm handler for the reviews_list form.
 */
function reviews_list_confirm_delete($form, &$form_state) {
  $form['reviews'] = array('#type' => 'value', '#value' => $form_state['values']['reviews']);
  $form['delete'] = array('#type' => 'value', '#value' => TRUE);
  return confirm_form($form,
    t('Are you sure you want to delete the selected reviews'),
    'admin/structure/reviews/list',
    t('Warning: This action cannot be undone and all deleted reviews will be permanently lost.'),
    t('Delete'),
    t('Cancel'));
}

/**
 * This function deletes the selected reviews
 */
function reviews_list_confirm_delete_submit($form, $form_state) {
  $reviews = $form_state['values']['reviews'];
  foreach ($reviews as $key => $review) {
    db_delete('reviews')
      ->condition('rid', $review, '=')
      ->execute();
  }
  drupal_set_message('The selected reviews have been deleted.', 'status');
}

/**
 * Form builder for reviews moderation.
 */
function reviews_moderation($form, &$form_state) {
  $form = array();

  $result = db_select('reviews', 'r')->extend('PagerDefault')->limit(15)
    ->fields('r')
    ->condition('status', 0, '=')
    ->orderBy('created', 'ASC')
    ->execute();

  $header = array(
    'rid' => array('data' => t('Review ID')),
    'node_title' => array('data' => t('Reviewed Content Title')),
    'username' => array('data' => t('Reviewer')),
    'review' => array('data' => t('Review')),
    'actions' => array('data' => t('Operations')),
  );

  $data = array();
  while ($review = $result->fetchAssoc()) {
    $review_content = unserialize($review['review']);
    $data[$review['rid']] = array(
      'rid' => $review['rid'],
      'node_title' => check_plain(_get_node_title($review['nid'])),
      'username' => check_plain(_get_username($review['uid'])),
      'review' => check_markup($review_content['value'], $review_content['format']),
      'actions' => l('view', 'node/'. $review['nid'] .'/reviews', array('fragment' => 'review_' . $review['rid'])) . ' / ' . l('approve', 'admin/structure/reviews/approve/' . $review['rid']),
    );
  }

  $form['reviews'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $data,
    '#empty' => t('There are currently no reviews awaiting moderation.'),
  );

  $form['pager'] = array(
    '#markup' => theme('pager', array('tags' => array())),
  );

  return $form;
}

/**
 * Function for menu callback to approve a review.
 */
function reviews_approve_review($rid) {
  db_update('reviews')
    ->fields(array('status' => 1))
    ->condition('rid', $rid, '=')
    ->execute();
  drupal_set_message('Review approved', 'status');
  drupal_goto('admin/structure/reviews/moderate');
}
