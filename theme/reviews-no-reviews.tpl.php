<?php

/**
 * @file
 * Theme template file used to format the reviews page of enabled content
 * types when there are no reviews.
 * $variables:
 *  nid: the node ID of the main node being viewed.
 */

?>
<div class="reviews no-reviews">
  <div class="clearfix">
    <p>
      There are currently no reviews for this content.
    </p>
    <p>
      Why not be the first to review it - <?php print l('click here', 'node/' . $variables['nid'] . '/add-review'); ?>
    </p>
  </div>
  <div class="reviews-back-to-page">
    <?php print l('Back to page', 'node/' . $variables['nid']); ?>
  </div>
</div>
