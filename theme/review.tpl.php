<?php

/**
 * @file
 * Theme template file used to format the reviews page of enabled content
 * types when there are reviews.
 * $variables:
 *  $nid: the node ID of the main node being viewed.
 *  $reviews: array of reviews
 *    $rid: review ID.
 *    $uid: user ID of reviewer.
 *    $review: review text.
 *    $created: timestamp of when the review was created.
 */

  $review = $variables['review'];
  $review_content = unserialize($review->review);
  $classes = '';

  if ($variables['index'] == 0) {
    $classes .= ' first';
  }

  if ($variables['index'] == (int)($variables['total_reviews'] - 1)) {
    $classes .= ' last';
  }

  if ($variables['index'] % 2 == 1) {
    $classes .= ' even';
  } else {
    $classes .= ' odd';
  }

  if ($review->status == 0) {
    $classes .= ' unpublished';
  }

?>
<div class="reviews-review <?php print $classes; ?>">
  <a name="review_<?php print $review->rid; ?>"></a>
  <div class="reviews-date-author">
    <span class="date"><?php print format_date($review->created, 'long'); ?></span> by <span class="author"><?php print _get_username($review->uid); ?></span>
  </div>
  <div class="reviews-content">
    <?php print check_markup($review_content['value'], $review_content['format']); ?>
  </div>
</div>
